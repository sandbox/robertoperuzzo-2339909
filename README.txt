CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------
This module generate your Google Shopping feed in one click, starting from
an eCommerce site based on Drupal Commerce. The Google products feed
specification are little bit complexes because they change by country;
with this module, you don’t need to worry about attributes requirements
but you focus on what you want to sell through your Google Merchant Center.


 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/robertoperuzzo/2339909


 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2339909


REQUIREMENTS
------------
This module requires the following modules:
 * Drupal Commerce (https://www.drupal.org/project/commerce)


RECOMMENDED MODULES
-------------------
 * Commerce Kickstart (https://www.drupal.org/project/commerce_kickstart)
   This Drupal distribution helps you to quickly get up and running with
   Drupal Commerce. The Google Shopping Feed current version is tested on
   Commerce Kickstart standard products type and display.


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * On the setting page /admin/commerce/config/advanced-settings/google-shopping
   insert the title and the description for your feed and choose which product
   you want to include into it.


TROUBLESHOOTING
---------------


FAQ
---


MAINTAINERS
-----------
Current maintainers:
 * Roberto Peruzzo (robertoperuzzo) - https://www.drupal.org/u/robertoperuzzo


This project has been sponsored by:
 * STUDIO AQUA
   Specialized in designing and developing Drupal powered sites, we believe that
   the online business is measurable for a environmentally sustainable Web.
   Visit http://www.studioaqua.it for more information.
